#include "mash.h"

[[distributable]]
/**
 * @brief Supervisory task
 * @details Not yet implemented
 * 
 * @param interface state server for receiving signals
 */

void supervisor_task(server interface state_i is) {
	while(1) {
		select{
			case is.signal(context_t * alias ctx) -> {state_t next_state, transition_t transition}:
				next_state = TOP;
				transition = NONE;
				debug_printf("[] Fall through, State %x|%x,Errors %x/%d\n",
											ctx->state, ctx->sig, ctx->errors, ctx->error);
				break;
		}
	}
}

inline static unsigned signal(context_t * alias ctx, unsigned instance,
													client interface state_i state_task[states], 
													unsigned states){
	state_t state;
	transition_t trans;
	unsigned reminder = 0;

	debug_printf("[MASH#%x] State %x|%x,Errors %x/%d\n",
											instance,ctx->state, ctx->sig, ctx->errors, ctx->error);
	//if(sig == TERMINATE) return;
	#ifdef FSM
	state = state_task[ctx->state].signal(ctx);
	if(ctx->state != state) { //it's a transition
		state_task[ctx->state].exit();
		state_task[state].entry();
		ctx->state = state;
	}
	#else
	{state,trans} = state_task[ctx->state].signal(ctx);
	switch(trans) { // perform external state transition
		case SIMPLE :
			ctx->state = state;
			break;
		case REMINDER :
			reminder = ctx->sig;
			ctx->state = state;
			break;
			// TODO add complex hierarchy transitions support when needed
		 default : // no external state transition (internal only), if not handled drill down hierarchy
			while (state && (state != ctx->state)) {
				if(state < states) {
					{state,trans} = state_task[state].signal(ctx);
					switch(trans) { // perform external state transition
						case SIMPLE :
							ctx->state = state;
							break;
						case REMINDER :
							reminder = ctx->sig;
							ctx->state = state;
							break;
					}
				}
			}
			break;
	}
	#endif
	return reminder;
}


[[combinable]]
void mash(server interface mash_i msh[Clients], const unsigned Clients, unsigned instance, context_t * alias ctx,
					client interface state_i state_task[states], unsigned states) {

	timer tmr;
	unsigned timeout,reminder = 1;


	debug_printf("[MASH#%x] Starting\n",instance);

	tmr :> timeout;	

	while(1) {
		select {
			case reminder => tmr when timerafter(timeout) :> void:
			  debug_printf("[MASH#%x] Timeout\n",instance);	
				ctx->sig = TIMEUP;
				reminder = signal(ctx,instance,state_task,states);
				tmr :> timeout;
				timeout += reminder;
				break;				
			case msh[int i].signal(signal_t sig) :
				ctx->sig = sig;
				reminder = signal(ctx,instance,state_task,states);
				tmr :> timeout;
				timeout += reminder;
				break;
			case msh[int i].message(unsigned buf[n],unsigned n): 
				if(n < MASHBUF) {
					memcpy(ctx->buf,buf,n*sizeof(unsigned));
					ctx->sig = RX;
					reminder = signal(ctx,instance,state_task,states);
					tmr :> timeout;
					timeout += reminder;
				} else {
					ctx->errors++;
					ctx->error = MSGERROR;
					debug_printf("[MASH#%x] Error, Message length exceeded\n",instance);
				}
				break;
		}
	}
}

