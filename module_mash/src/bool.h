#ifndef _mbool_h_
#define _mbool_h_

#define TRUE  1
#define FALSE 0

typedef enum bool_e {false = 0,true = 1} bool_t;

#endif