#ifndef MASH_H
#define MASH_H

#include <platform.h>
#define DEBUG_UNIT MODULE_MASH
#define DEBUG_PRINT_ENABLE_MODULE_MASH 1
#include "debug_print.h"
#include "bool.h"
#include <string.h>

#ifndef MASHBUF
#define MASHBUF 40
#endif

typedef enum {TOP,SUPER,INITIAL,STATE1} state_t; 
typedef enum {TERMINATE,ERROR,INITIALISE,RX,TIMEUP,SIG1} signal_t;
typedef enum {NONE,SIMPLE,REMINDER} transition_t;
typedef enum {NOERROR,MSGERROR,GENERROR} error_t;

/*
typedef struct cmd_buf_s {
	unsigned char buf[MAXCMD];
  unsigned len;
  unsigned pos;
  unsigned src;
} cmd_buf_t;
*/

typedef struct context_s {
	state_t state;
	signal_t sig;
	//cmd_buf_t cmd;
	error_t error; 
	unsigned errors;
	unsigned buf[MASHBUF];
} context_t;


interface state_i {
	#ifdef FSM
	void exit(void);
	void entry(void);
	state_t signal(context_t * alias ctx);
	#else
	{state_t,transition_t} signal(context_t * alias ctx);
	#endif
};

interface mash_i {
	void message(unsigned buf[n],unsigned n);
	void signal(signal_t sig);
};

[[distributable]]
/**
 * @brief Supervisory task
 * @details Not yet implemented
 * 
 * @param interface state server for receiving signals
 */
void supervisor_task(server interface state_i is);

[[combinable]]
/**
 * @brief MASH FSM/HSM state machine operator runs using RTC semantics
 * @details This is a very simple HSM/FSM framework for XC based projects, include this task
 * 
 * @param interface mash_i for incoming signals (from outside state machine)
 * @param interface state_i for sending signals to states, each state task needs to implement this
 * @param states number of state implemented terminate state  enumeration with FINAL and pass this in
 */
void mash(server interface mash_i msh[Clients],const unsigned Clients, unsigned instance, context_t * alias ctx,
					client interface state_i state_task[states],unsigned states);

#endif
