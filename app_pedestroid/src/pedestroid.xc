#include "pedestroid.h"

[[distributable]]
/**
 * @brief This is the HSM superstate, it handles signals that the other state classes don't
 * 
 * @param interface state server for receiving signals
 * @param interface Pedestrian client driver, allowing us to command it
 */
void super_state_task(server interface state_i is,client interface pedestrian_driver pd){
	while(1){
		select{
			case is.signal(context_t * alias ctx) -> {state_t next_state, transition_t transition}:
				next_state = TOP;
				transition = NONE;
				switch(ctx->sig) {
					case TIMEUP : 
						pd.set(((pedctx_t * alias)ctx)->color);
						transition = REMINDER;
						ctx->sig = TFL_DELAY;
						next_state = ctx->state;
						break;
					default :
						//signal not handled, should be an error
						debug_printf("[SUPER] Signal ignored: %d\n",ctx->sig);
						break;
				} 
				break;
		}
	}
}

[[distributable]]
/**
 * @brief Initial state task only used once on entry to state machine
 * 
 * @param interface state server for receiving signals
 * @param interface Pedestrian client driver, allowing us to command it
 */
void initial_state_task(server interface state_i is, client interface pedestrian_driver pd) {    
	
	while(1){
		select{
			case is.signal(context_t * alias ctx) -> {state_t next_state, transition_t transition}:
				next_state = ctx->state;;
				transition = NONE;
				switch(ctx->sig) { // make this default behaviour no switch
					case TIMEUP : 
						debug_printf("[PED] Initialising\n");
						((pedctx_t * alias)ctx)->color = RED;
						pd.set(((pedctx_t * alias)ctx)->color);
						ctx->sig = TFL_DELAY;
						next_state = STARTING;
						transition = REMINDER;
						break;
				} 
			break;
		}
	}
}

[[distributable]]
/**
 * @brief Starting state holds the green for a period preventing premature crossing
 * 
 * @param interface state server for receiving signals
 * @param interface Pedestrian client driver, allowing us to command it
 */
void starting_state_task(server interface state_i is) {
	while(1) {
		select { 
			case is.signal(context_t * alias ctx) -> {state_t next_state, transition_t transition}:
				next_state = SUPER;
				transition = NONE;
				switch(ctx->sig) {
					case TIMEUP :
						ctx->state = FLOWING;
						((pedctx_t * alias)ctx)->color = GREEN;
						break;
				} 
			break;
		}
	}
}

[[distributable]]
/**
 * @brief Flowing state, this is the main allowing the traffic to flow, it waitrs for pedestriian cross request
 * 
 * @param interface state server for receiving signals
 * @param interface Pedestrian client driver, allowing us to command it
 */
void flowing_state_task(server interface state_i is,client interface pedestrian_driver pd) {
	while(1) {
		select { 
			case is.signal(context_t * alias ctx) -> {state_t next_state, transition_t transition}:
				next_state = ctx->state;
				transition = NONE;
				switch(ctx->sig) {
					case TIMEUP :
						pd.arm();
						((pedctx_t * alias)ctx)->color = GREEN;
						pd.set(((pedctx_t * alias)ctx)->color);
						break;
					case CROSS : 
						ctx->sig= TFL_DELAY;
						next_state = WAITING;
						transition = REMINDER;
						break;
				} 
			break;
		}
	}
}

[[distributable]]
/**
 * @brief Waiting state, activated after pedsestrian crossing request, starts crossing sequence
 * 
 * @param interface state server for receiving signals
 * @param interface Pedestrian client driver, allowing us to command it
 */
void waiting_state_task(server interface state_i is) {
	while(1) {
		select { 
			case is.signal(context_t * alias ctx) -> {state_t next_state, transition_t transition}:
				next_state = SUPER;
				transition = NONE;
				switch(ctx->sig) {
					case TIMEUP :
						ctx->state = CHANGING;
						((pedctx_t * alias)ctx)->color = GREEN;
						break;
				} 
			break;
		}
	}
}

[[distributable]]
/**
 * @brief Changing state indicating to traffic that it is about to be halted
 * 
 * @param interface state server for receiving signals
 * @param interface Pedestrian client driver, allowing us to command it
 */
void changing_state_task(server interface state_i is) {
	while(1) {
		select { 
			case is.signal(context_t * alias ctx) -> {state_t next_state, transition_t transition}:
				next_state = SUPER;
				transition = NONE;
				switch(ctx->sig) {
					case  TIMEUP :
						ctx->state = STOPPING;
				    ((pedctx_t * alias)ctx)->color = AMBER;
						break;
				} 
			break;
		}
	}
}

[[distributable]]
/**
 * @brief Stopping state halts the traffic prior to enabling pedestrian crossing
 * 
 * @param interface state server for receiving signals
 * @param interface Pedestrian client driver, allowing us to command it
 */
void stopping_state_task(server interface state_i is) {
	while(1) {
		select { 
			case is.signal(context_t * alias ctx) -> {state_t next_state, transition_t transition}:
				next_state = SUPER;
				transition = NONE;
				switch(ctx->sig) {
					case TIMEUP :
						ctx->state = CROSSING;
						((pedctx_t * alias)ctx)->color = RED;
						break;
				} 
			break;
		}
	}
}

[[distributable]]
/**
 * @brief Crossing state activates 'WALK' mode allowing pedestrians to cross
 * 
 * @param interface state server for receiving signals
 * @param interface Pedestrian client driver, allowing us to command it
 */
void crossing_state_task(server interface state_i is,client interface pedestrian_driver pd) {
	while(1) {
		select { 
			case is.signal(context_t * alias ctx) -> {state_t next_state, transition_t transition}:
				next_state = ctx->state;
				transition = NONE;
				switch(ctx->sig) {
					case TIMEUP :
						((pedctx_t * alias)ctx)->color = RED | WALK;
						pd.set(((pedctx_t * alias)ctx)->color);
						pd.sound(SLOW);
						ctx->sig= TFL_DELAY;
						next_state = WARNING;
						transition = REMINDER;
						break;
				} 
			break;
		}
	}
}

[[distributable]]
/**
 * @brief Warning state flashes the walk indicator to warn that walking is about to end 
 * 
 * @param interface state server for receiving signals
 * @param interface Pedestrian client driver, allowing us to command it
 */
void warning_state_task(server interface state_i is,client interface pedestrian_driver pd) {
	unsigned flashes = FLASHES;
	unsigned sound = SLOW;

	while(1) {
		select { 
			case is.signal(context_t * alias ctx) -> {state_t next_state, transition_t transition}:
				next_state = ctx->state;
				transition = NONE;
				pedctx_t * alias pctx = (pedctx_t * alias)ctx;
				switch(ctx->sig) {
					case TIMEUP :
						pctx->color = (pctx->color == RED) ? RED | WALK : RED;
						pd.set(pctx->color);
						sound = sound ? SLOW : FAST;
						pd.sound(sound);
						if(!flashes--) {
							flashes = FLASHES;
							next_state = STARTING;
						}
						//ctx->sig= FLASH_DELAY;
						transition = REMINDER;
						break;
				} 
			break;
		}
	}
}


void pedestroid_active_task(void) {
	interface mash_i msh[1];
	interface state_i is[FINAL];
	interface pedestrian_driver pd[5];

	pedctx_t context = {{INITIAL,INITIALISE,NOERROR,0,{0}},0};

	[[combine]]
	par {
		// Driver tasks
		pedestrian_driver_task(pd,5,msh[0]);
		// MASH Handler Task
		mash(msh,1,1,(context_t * alias)&context,is,FINAL);
		// Supervisor task
		supervisor_task(is[TOP]);
		// State Tasks STARTING,FLOWING,WAITING,CHANGING,STOPPING,CROSSING,WARNING,FINAL
		super_state_task(is[SUPER],pd[0]);
		
		initial_state_task(is[INITIAL],pd[1]);
		starting_state_task(is[STARTING]);
		flowing_state_task(is[FLOWING],pd[2]);
		waiting_state_task(is[WAITING]);
		changing_state_task(is[CHANGING]);
		stopping_state_task(is[STOPPING]);
		crossing_state_task(is[CROSSING],pd[3]);
		warning_state_task(is[WARNING],pd[4]);
	}
}
