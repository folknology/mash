#include "driver.h"

#define CROSSING_DELAY 1000000000
#define TFL_DELAY 500000000
#define FLASH_DELAY 10000000
#define FLASHES 30

enum {STARTING = STATE1,FLOWING,WAITING,CHANGING,STOPPING,CROSSING,WARNING,FINAL};
/**
 * @brief The integrated active task for pedestroid incl state chart
 * @details  Comprising of state tasks forming the state chart along 
 * with a hw driver & mash handler tasks
 */
void pedestroid_active_task(void);
