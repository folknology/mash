#ifndef PED_DRIVER_h
#define PED_DRIVER_h

#include <xs1.h>
#include <platform.h>
#include "debug_print_conf.h"
#include "mash.h"

typedef enum {RED=(unsigned)(1 << 3),
							AMBER=(unsigned)((1 << 3) | (1 << 2)),
							GREEN=(unsigned)(1 << 2),
							WALK=(unsigned)(1)
} colors;

typedef enum {SLOW,FAST} sounds;

enum {GO = SIG1,CROSS};

interface pedestrian_driver {
	void set(colors colour);
	void sound(sounds sound);
	void arm(void);
};

typedef struct ped_s {
	out port lights;
	out port buzzer;
	in port button;
} ped_t;

typedef struct pedctx_s {
	context_t ctx;
	int color;
} pedctx_t;

[[combinable]]
/**
 * @brief Pedestrian hardware driver
 * @details Pedestrian crossing hardware events management, timers, GPIOs and interface
 * 
 * @param interface Pedestrian server driver receives commands from state layer
 * @param interface Mash client receiver to stimulate state machine
 * @param ped Structure containiong port resources for the traffic lights and indicators
 */
void pedestrian_driver_task(server interface pedestrian_driver pd[Clients],
														const unsigned Clients,client interface mash_i msh);

#endif
