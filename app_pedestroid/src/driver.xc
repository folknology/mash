#include "driver.h"

ped_t ped = {on tile[0]:XS1_PORT_4F,on tile[0]:XS1_PORT_1J,on tile[0]:XS1_PORT_4E};

[[combinable]]
/**
 * @brief Pedestrian hardware driver
 * @details Pedestrian crossing hardware events management, timers, GPIOs and interface
 * 
 * @param interface Pedestrian server driver receives commands from state layer
 * @param interface Mash client receiver to stimulate state machine
 * @param ped Structure containiong port resources for the traffic lights and indicators
 */
void pedestrian_driver_task(server interface pedestrian_driver pd[Clients],
														const unsigned Clients,client interface mash_i msh) {
	// local resources and variables
	unsigned buttons;
	bool_t pressed = false;

	// reset lights/indicators
	ped.lights <: 0x0000;
	ped.button :> buttons;
	debug_printf("[DRVR] Initial Buttons %u\n",buttons);

	while(1){
		select{

			case pd[int i].set(colors color):
				ped.lights <: color;
				break;
			case pd[int i].sound(sounds sound):
				ped.buzzer <: 1;
				break;
			case pd[int i].arm(void):
				pressed = false;
				break;
			case !pressed => ped.button when pinsneq(0x3) :> buttons:
				debug_printf("[DRVR] Buttons %u\n",buttons);
				pressed = true;
				msh.signal(CROSS);
				break;
		}
	}
}

